//
//  TopMenuItemsView.swift
//  MySmart
//
//  Created by Janus fidel Balatbat on 2/29/16.
//  Copyright Â© 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit


@objc protocol MenuItemDelegate
{
    func itemSelectedWithIndex(selectedItemIndex: NSInteger, item:MenuItem)
}

class MenuItem: UIView {
    internal var selectedColor = UIColor.blackColor()
    internal var delegate : MenuItemDelegate?
    internal var index = 0
    internal var label : UILabel!
    internal var normalColor = UIColor.whiteColor(){
        didSet{
            self.label.textColor = normalColor
        }
    }
    internal var font = UIFont.systemFontOfSize(12){
        didSet{
            self.label.font = font
        }
    }
    var title = String(){
        didSet{
            self.label.text = self.title
        }willSet{
            
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = frame
        self.initialize()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initialize()
    }
    convenience init(menuTitle : String) {
        self.init(frame: CGRectZero)
        self.initialize()
    }
    func initialize() {
        let tapReco = UITapGestureRecognizer(target: self, action: #selector(MenuItem.onTapDetected))
        tapReco.numberOfTapsRequired = 1
        tapReco.numberOfTouchesRequired = 1
        self.addGestureRecognizer(tapReco)
        self.configContent()
    }
    //MARK: Content + Actions
    private func configContent(){
        label = UILabel(frame: CGRectMake(0,0,frame.width,frame.height))
        label.font = self.font
        label.numberOfLines = 2;
        label.minimumScaleFactor = 0.8;
        
        label.textAlignment =  NSTextAlignment.Center
        label.textColor = self.normalColor
        label.backgroundColor = UIColor.clearColor()
        self.addSubview(label)
    }
    func addLine(){
        let line = CAShapeLayer()
        line.lineWidth = 5
        let linePath = UIBezierPath()
        linePath.moveToPoint(CGPointMake(2, self.bounds.size.height))
        linePath.addLineToPoint((CGPointMake(self.bounds.size.width - 2, self.bounds.size.height)))
        line.path = linePath.CGPath
        line.fillColor = UIColor.redColor().CGColor
        line.opacity = 1.0
        line.strokeColor = UIColor.blueColor().CGColor
        self.layer.addSublayer(line)
        
    }
    func onTapDetected(){
        
        self.label.textColor = selectedColor
        if let del = delegate{
            del.itemSelectedWithIndex(self.index, item: self)

        }
    }
    
}
//
//  TopMenuBar.swift
//  MySmart
//
//  Created by Janus fidel Balatbat on 2/29/16.
//  Copyright Â© 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit

struct MenuBarStyleKeys {
    static let Font = "kFont"
    static let NormalColor = "kNormalColor"
    static let SelectedFont = "kSelectedFont"
    static let SelectedColor = "kSelectedColor"
    static let SpacerColor = "kSpacerColor"
    static let BackgroundColor = "kBackgroundColor"
}

@objc protocol TopMenuBarDelegate{
    func widthForItemAtIndex(index: NSInteger) ->CGFloat
    func selectedItemAtIndex(index: NSInteger,fromBar: TopMenuBar)
    optional func backgroundColorAtIndex(index: NSInteger, inBar: TopMenuBar)-> UIColor
    optional func spacerWidthAtIndex(index: NSInteger) -> CGFloat
    optional func separatorColor()->UIColor
}

class TopMenuBar: UIView, MenuItemDelegate {
    var hasLine = false
    var menuItemRef = [MenuItem]()
    var style = [String : Any](){
        didSet{
            self.updateItemStyle()
            self.updateBar()
        }
    }
    var view: UIView!
    @IBOutlet weak var scrollView : UIScrollView!
    var  delegate : TopMenuBarDelegate?
    
    var separatorColor = UIColor.blackColor()
    var bounces = true
    var selectedItemIndex = 0{
        didSet{
        }
    }
    
    var menuBarWidth : CGFloat = 0.0;
    var items = [String](){
        didSet{
            self.configMenu()
        }
    }
    //MARK: Initializers
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        setUp()
    }
    func setUp() {
        view = loadNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        self.view.clearsContextBeforeDrawing = false
        addSubview(view)
    }
    func loadNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "TopMenuBar", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    //MARK: Selection
    func selectItemAtIndex(index: NSInteger, fromDelegate:Bool){
        if fromDelegate == true{
            for (i,item) in self.menuItemRef.enumerate(){
                if i == index{
                    item.label.textColor =  item.selectedColor
                }
            }
            self.deselectItemsAgainstItemWithIndex(index)
            self.selectedItemIndex = index
            self.handleScrolling()
        }
        self.setNeedsDisplay()
        
    
    }
    
    func deselectItemsAgainstItemWithIndex(index: NSInteger){
        for (i,item) in self.menuItemRef.enumerate(){
            if i != index{
                let tempItem = item;
                item.label.textColor =  tempItem.normalColor
            }else{
                
            }
        }
    }
    
    //MARK:  Styling
    func updateItemStyle(){
        for item in self.menuItemRef{
            self.applyStyleToItem(item)
        }
    }
    func applyStyleToItem(item:MenuItem){
        if let font = style[MenuBarStyleKeys.Font] as? UIFont{
            item.font = font
        }
        if let normalColor = style[MenuBarStyleKeys.NormalColor] as? UIColor{
            item.normalColor = normalColor;
        }
        if let selectedColor = style[MenuBarStyleKeys.SelectedColor] as? UIColor{
            item.selectedColor = selectedColor
        }
        
    }
    func updateBar(){
        if let bg = style[MenuBarStyleKeys.BackgroundColor] as? UIColor{
            self.scrollView.backgroundColor = bg;
        }
    }
    
    //MARK: View Configuration
    func configMenu(){
        if self.menuItemRef.count > 0{
            //reset reference before proceeding
            menuItemRef.removeAll(keepCapacity: false)
            for item in scrollView.subviews {
                item.removeFromSuperview()
            }
        }
        var x : CGFloat = 0.0;
        let scrollViewHeight = self.scrollView.frame.size.height
        for (index, title) in items.enumerate(){
            //width for current item set in delegate
            let width = (self.delegate?.widthForItemAtIndex(index))!
            
            //spacer widht set in delegate if any
            var spacerWidth : CGFloat = 0.0
            if let spacerW = delegate?.spacerWidthAtIndex?(index){
                spacerWidth = spacerW
            }
            
            self.menuBarWidth += (self.delegate?.widthForItemAtIndex(index))!
            let menuItem = MenuItem(frame: CGRectMake(0,0,(self.delegate?.widthForItemAtIndex(index))!,self.bounds.height))
            menuItem.title = title
            menuItem.index = index
            menuItem.delegate  = self
            self.menuItemRef.append(menuItem)
            self.applyStyleToItem(menuItem)
            
            //check if item first or last for positioning
            let spacerW = (index == 0 || index == self.items.count - 1) ? 0.0 : spacerWidth ;
            menuItem.frame = CGRectMake(x + spacerW, 0, width, scrollViewHeight)
            
            //background color set in delegate if any
            let bg = self.delegate?.backgroundColorAtIndex?(index, inBar: self)
            menuItem.backgroundColor = UIColor.clearColor()
            if let bgColor = bg{
                menuItem.backgroundColor = bgColor
            }
            
            //add to scrollview
            scrollView.addSubview(menuItem)
            
            //adding separator
            if index != 0 || index == self.items.count - 1{
                self.menuBarWidth += spacerW
                if let sepColor = delegate?.separatorColor?(){
                    self.separatorColor = sepColor;
                }
                let spacer = UIView(frame: CGRectMake(x ,0,spacerWidth,scrollViewHeight))
                spacer.backgroundColor = self.separatorColor
                scrollView.addSubview(spacer)
                x += spacerW
                
            }
            
            x += (self.delegate?.widthForItemAtIndex(index))!
        }
        self.scrollView.contentSize = CGSizeMake(x, 0)
        
        
    }
    //MARK: Menu Item
    func itemSelectedWithIndex(selectedItemIndex: NSInteger, item: MenuItem) {

        if let del = delegate{
            del.selectedItemAtIndex(selectedItemIndex, fromBar: self)
        }
        self.deselectItemsAgainstItemWithIndex(selectedItemIndex)
        self.selectedItemIndex = selectedItemIndex
        self.handleScrolling()
        
        self.setNeedsDisplay()
    }
    //MARK: Scroll handler
    func handleScrolling(){
        var targetX = self.getCenterLocForItemAtIndex(self.selectedItemIndex) - (scrollView.bounds.size.width / 2) + (self.delegate?.widthForItemAtIndex(self.selectedItemIndex))!
        if targetX < 0{
            targetX = 0
        }
        if (targetX >  menuBarWidth - scrollView.bounds.size.width){
            targetX =  menuBarWidth  - scrollView.bounds.size.width
        }
        if !(scrollView.bounds.size.width >  menuBarWidth ){
            self.scrollView.setContentOffset(CGPointMake(targetX  , 0), animated: true)
        }
    }
    
    func getCenterLocForItemAtIndex(itemIndex: NSInteger) -> CGFloat{
        var currentX = CGRectGetMinX(self.bounds)
        for i in 0...itemIndex{
            let itemWidth = (self.delegate?.widthForItemAtIndex(i))!
            currentX += itemWidth
            if (i == itemIndex){
                currentX =  currentX - (itemWidth / 2)
            }
        
        }
        return CGFloat(floorf(Float(currentX)))
        
    }
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        //clear previous layers
        if let sublayers = self.scrollView.layer.sublayers {
            for layer in sublayers {
                if layer.isKindOfClass(CAShapeLayer) && layer.name == "horizontalLine"
                {
                    layer.removeFromSuperlayer()
                }
            }
        }
        
        var selectedItem = MenuItem()
        // get the selected item
        for (i,item) in self.menuItemRef.enumerate(){
            if self.selectedItemIndex == i{
                selectedItem = item
                break
            }
        }
        let selectedItemFrame = selectedItem.frame
        //add line
        let line = CAShapeLayer()
        line.name = "horizontalLine"
        line.lineWidth = 5
        let linePath = UIBezierPath()
        linePath.moveToPoint(CGPointMake(selectedItemFrame.origin.x, self.bounds.size.height))
        linePath.addLineToPoint((CGPointMake(selectedItemFrame.origin.x + selectedItemFrame.size.width, self.bounds.size.height)))
        line.path = linePath.CGPath
        line.fillColor = selectedItem.selectedColor.CGColor
        line.opacity = 1.0
        line.strokeColor = selectedItem.selectedColor.CGColor
        self.scrollView.layer.addSublayer(line)
        
    }
    //MARK: Layout
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    
}
//
//  ViewController.swift
//  SampleProj
//
//  Created by Janus Fidel Balatbat on 9/15/16.
//  Copyright © 2016 Janus Fidel Balatbat. All rights reserved.
//

import UIKit

class ViewController: UIViewController,TopMenuBarDelegate {
    @IBOutlet weak var topMenuView : TopMenuBar!
    @IBOutlet weak var scrollView: UIScrollView!
    var didAddContent = false
    let menuItems = ["Menu 1","Menu 2","Menu 3","Menu 4","Menu 5","Menu 6","Menu 7"];
    override func viewDidLoad() {
        super.viewDidLoad()
        //set up topmenubar
        topMenuView.delegate = self
        topMenuView.style = [MenuBarStyleKeys.NormalColor : UIColor.blackColor(),
                             MenuBarStyleKeys.Font :UIFont.systemFontOfSize(17),
                             MenuBarStyleKeys.SelectedColor : UIColor.whiteColor(),
                             MenuBarStyleKeys.BackgroundColor : UIColor.clearColor()]
        self.topMenuView.items = menuItems
        self.topMenuView.selectItemAtIndex(0, fromDelegate: true)
     
    }
    func addContentViews(){
        if didAddContent == true{
            return
        }
        didAddContent = true
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        var scrollViewCSwidth : CGFloat = 0.0;
        for menu in menuItems{
            let view = UIView(frame: CGRectZero)
            view.backgroundColor = UIColor.redColor()
            view.frame = CGRectMake(scrollViewCSwidth, 0, scrollView.frame.size.width, scrollView.frame.size.height)
            scrollViewCSwidth += self.scrollView.frame.size.width
            let title = UILabel(frame: CGRectMake(0,0,100,20))
            title.text = menu
            title.textAlignment =  .Center
            title.center = CGPoint(x: view.frame.size.width / 2, y: view.frame.size.height / 2)
            view.addSubview(title)
            self.scrollView.addSubview(view)
        }
        self.scrollView.contentSize = CGSizeMake(scrollViewCSwidth, 0)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //need final view before loading contents
        self.addContentViews()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UIScrollViewDelegates
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.topMenuView.selectItemAtIndex(Int(pageNumber), fromDelegate: true)
        
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
    }
    //MARK: TopBarDelegate
    func widthForItemAtIndex(index: NSInteger) -> CGFloat {
        return 120
    }
    func selectedItemAtIndex(index: NSInteger, fromBar: TopMenuBar) {
        self.scrollView.setContentOffset(CGPointMake(self.scrollView.frame.size.width * CGFloat(index), 0), animated: true)
    }
    func backgroundColorAtIndex(index: NSInteger, inBar: TopMenuBar) -> UIColor {
        return UIColor.clearColor()
    }
    
    func spacerWidthAtIndex(index: NSInteger) -> CGFloat {
        return 0
    }
    func separatorColor() -> UIColor {
        return UIColor.redColor()
    }
}

